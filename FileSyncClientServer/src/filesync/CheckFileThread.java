package filesync;

import java.io.File;
import java.io.IOException;

/**
 * Thread that checks if the file is modified and updates the instruction buffer
 * if it is. Thread is expected to be run on the source side
 * @author jordansteele
 *
 */

public class CheckFileThread implements Runnable {
	protected File file; // the actual file object used to check modification date
	protected SynchronisedFile syncFile; 
	
	public CheckFileThread(SynchronisedFile syncFile) {
		this.syncFile = syncFile;
		this.file = new File(syncFile.getFilename());
	}
	
	@Override
	public void run() {
		long prevLastModified = -1;
		while(true){
			try {
				// If file has been modifed, check file state
				if (prevLastModified != file.lastModified()) {
					prevLastModified = file.lastModified();
					System.err.println("SynchTest: calling fromFile.CheckFileState()");
					syncFile.CheckFileState();
				}
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(-1);
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.exit(-1);
			}
			
			// wait 5 seconds
			wait(5000);
		}
	}
	
	/**
	 * Wrapper method for exception handling for Thread.sleep
	 * @param time Time in millis to sleep for
	 */
	private void wait(int time) {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}
}
