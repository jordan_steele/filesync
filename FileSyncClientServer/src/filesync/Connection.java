package filesync;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * Abstract class to keep the variables for connections in one place
 * @author jordansteele
 *
 */

public abstract class Connection implements Runnable {
	protected DataInputStream in;
	protected DataOutputStream out;
	protected Socket socket;
	
	public Connection (Socket socket) {
		try {
			this.socket = socket;
			this.in = new DataInputStream(socket.getInputStream());
			this.out = new DataOutputStream(socket.getOutputStream());
		} catch(IOException e) {
			System.out.println("Connection:"+e.getMessage());
		}
	}
}
