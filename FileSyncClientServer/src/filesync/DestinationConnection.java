package filesync;

import java.io.IOException;
import java.net.Socket;

/**
 * Thread to be run on the destination side of a file sync. Simply receives 
 * JSON messages, and processes the instruction they outline. Either responds
 * with a instruction processed response or requests a new block if required
 * @author jordansteele
 *
 */

public class DestinationConnection extends FileSyncConnection {
	
	public DestinationConnection(SynchronisedFile syncFile, Socket socket) {
		super(syncFile, socket);
	}
	
	@Override
	public void run() {	
		String msg;
		try {
			while ((msg = in.readUTF()) != null) {
				handleMessage(msg);
			}
		} catch (IOException e) {
			System.out.println("destination: " + e.getMessage());
			System.exit(-1);
		}
	}
	
	/**
	 * Handles a message received from the source by processing the instruction
	 * it details and responding.
	 * @param msg The message from the source
	 */
	private void handleMessage(String msg) {
		Instruction receivedInst = new InstructionFactory().FromJSON(msg);		
		try {
			syncFile.ProcessInstruction(receivedInst);
			respondWithResult(FileSyncCommand.INSTRUCTION_PROCESSED);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1); // just die at the first sign of trouble
		} catch (BlockUnavailableException e) {
			respondWithResult(FileSyncCommand.REQUEST_NEW_BLOCK);
		}
	}
	
	/**
	 * Takes a command to respond with and sends to the source
	 * @param command Command to send to the source
	 */
	private void respondWithResult(FileSyncCommand command) {
		try {
			out.writeUTF(command.name());
		} catch (IOException e) {
			System.out.println("response: " + e.getMessage());
		}
	}
}
