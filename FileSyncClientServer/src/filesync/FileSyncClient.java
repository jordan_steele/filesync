package filesync;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import org.kohsuke.args4j.Option;

/**
 * Client used in a FileSync
 * @author jordansteele
 *
 */

public class FileSyncClient extends FileSyncComponent {
	
	@Option(name="-file", usage="file to sync with the server")
	private String syncFilename;
	
	@Option(name="-host", usage="hostname of the server to connect to")
	private String hostname;
	
	@Option(name="-direction", usage="use 'push' to make the client the source and 'push' to make the client the destination")
	private Direction direction;
	
	@Option(name="-blocksize", usage="the size in bytes of a block in the block map of a file")
	private int blockSize;
	
	public static void main (String args[]) {
		new FileSyncClient().doMain(args);
	}
	
	/**
	 * Does the main actions on an actual object rather than as static, 
	 * this is required for args4j
	 * @param args The command line arguments
	 */
	public void doMain(String[] args) {
		parseArgs(this, args);
		startClient(hostname, FileSyncServer.port, 
				createSyncFile(syncFilename, fileSyncConditions.getBlockSize()));
	}
	
	/**
	 * Overrides the superclass method to init the file sync conditions as well
	 */
	@Override
	protected void parseArgs(Object mainObject, String[] args) {
		super.parseArgs(mainObject, args);
		fileSyncConditions = new FileSyncConditions(direction, blockSize);
	}
	
	/**
	 * Starts the client, sends the file sync conditions to be used to the server,
	 * starts itself as a source or a destination depending on the direction
	 * @param hostname Hostname of the server
	 * @param port The port that the server communicates on
	 * @param syncFile The file to be synchronised
	 */
	private void startClient(String hostname, int port, SynchronisedFile syncFile) {
		Socket socket = createSocket(hostname, port);
		sendFileSyncConditions(socket);
		startConnection(syncFile, socket);
	}
	
	/**
	 * Creates a new socket to communicate with the given hostname and port
	 * @param hostname Hostname to use in socket creation
	 * @param port Port to use in socket creation
	 * @return The socket or null if creating the socket failed
	 */
	private Socket createSocket(String hostname, int port) {
		Socket socket = null;
		try {
			socket = new Socket(hostname, port);
		} catch(UnknownHostException e) {
			System.err.println("Socket:" + e.getMessage());
			System.exit(-1);
		} catch (IOException e){
			System.err.println("Socket:" + e.getMessage());
			System.exit(-1);
		}
		return socket;
	}

	/**
	 * Sends the conditions for the current FileSync to the server
	 * @param socket The socket to send the conditions along
	 */
	private void sendFileSyncConditions(Socket socket) {
		try { 
			DataOutputStream out = new DataOutputStream( socket.getOutputStream());
			out.writeUTF(fileSyncConditions.toJSON());
		} catch (IOException e) {
			System.err.println("sending file sync conditions" + e.getMessage());
			System.exit(-1);
		}
	}
	
	/**
	 * Starts the client as a source of destination depending on the direction condition
	 * @param syncFile The file to be synchronised
	 * @param socket The socket to communicate with the server
	 */
	private void startConnection(SynchronisedFile syncFile, Socket socket) {
		switch (fileSyncConditions.getDirection()) {
			case PUSH:
				startSource(syncFile, socket);
				break;
			case PULL:
				startDestination(syncFile, socket);
				break;
			default:
				System.err.println("No such direction.");
				System.exit(-1);
				break;
		}
	}
}
