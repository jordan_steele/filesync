package filesync;

import java.io.IOException;
import java.net.Socket;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

/**
 * Abstract class that is intended to be extended by clients and servers
 * @author jordansteele
 *
 */

public abstract class FileSyncComponent {
	
	protected FileSyncConditions fileSyncConditions;
	
	enum Direction {
		PUSH, 
		PULL
	}
	
	/**
	 * Uses args4j library to parse the commond line arguments
	 * @param mainObject The main object whose args are to parsed
	 * @param args The actual command line arguments
	 */
	protected void parseArgs(Object mainObject, String[] args) {
		CmdLineParser parser = new CmdLineParser(mainObject);
        try {
            parser.parseArgument(args);

        } catch( CmdLineException e ) {
            System.err.println(e.getMessage());
            System.err.println("java SampleMain [options...] arguments...");
            parser.printUsage(System.err);
            System.err.println();
            return;
        }
	}
	
	/**
	 * Creates a new SynchronisedFile and handles exceptions
	 * @param syncFilename Name of the file to be synced
	 * @param blockSize The size in bytes of a block
	 * @return The newly created SynchronisedFile
	 */
	protected SynchronisedFile createSyncFile(String syncFilename, int blockSize) {
		SynchronisedFile syncFile=null;
		try {
			syncFile = new SynchronisedFile(syncFilename, blockSize);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		return syncFile;
	}
	
	/**
	 * Starts the threads required for the component to act as the source
	 * @param syncFile The file being synchronised
	 * @param socket The socket for communication
	 */
	protected void startSource(SynchronisedFile syncFile, Socket socket) {
		new Thread(new CheckFileThread(syncFile)).start();
		new Thread(new SourceConnection(syncFile, socket)).start();
	}
	
	/**
	 * Starts the thread required for the component to act as the destination
	 * @param syncFile The file being synchronised
	 * @param socket The socket for communication
	 */
	protected void startDestination(SynchronisedFile syncFile, Socket socket) {
		new Thread(new DestinationConnection(syncFile, socket)).start();
	}
	
	/**
	 * Simple inner class to store the conditions of the current synchronisation
	 * and provide a toJSON and fromJSON to allow easy communication of these conditions
	 * @author jordansteele
	 *
	 */
	public class FileSyncConditions {
		private Direction direction;
		private int blockSize;
		
		public FileSyncConditions(){}
		
		public FileSyncConditions(Direction direction, int blockSize) {
			this.direction = direction;
			this.blockSize = blockSize;
		}

		/**
		 * Transforms the condition variables to a JSON string
		 * @return The conditions represented as a JSON string
		 */
		@SuppressWarnings("unchecked")
		public String toJSON() {
			JSONObject obj = new JSONObject();
			obj.put("direction", direction.toString());
			obj.put("blocksize", blockSize);
			return obj.toJSONString();
		}
		
		/**
		 * Takes a JSON string and sets the objects variables to the values
		 * specified in the string
		 * @param jsonString A JSON string storing the values to set to
		 */
		public void fromJSON(String jsonString) {
			JSONObject obj=null;
			JSONParser parser = new JSONParser();
			try {
				obj = (JSONObject) parser.parse(jsonString);
			} catch (ParseException e) {
				e.printStackTrace();
				System.exit(-1);
			}
			if(obj!=null){
				direction = Direction.valueOf((String) obj.get("direction"));
				blockSize = ((Long) obj.get("blocksize")).intValue();
			}
		}
		
		/** Getters and setters */
		
		public Direction getDirection() {
			return direction;
		}

		public int getBlockSize() {
			return blockSize;
		}
	}
}
