package filesync;

import java.net.Socket;

import filesync.SynchronisedFile;

/**
 * Abstract class to store common variables in a connection for file syncing
 * @author jordansteele
 *
 */

public abstract class FileSyncConnection extends Connection {
	protected SynchronisedFile syncFile;
	
	enum FileSyncCommand {
		INSTRUCTION_PROCESSED,
		REQUEST_NEW_BLOCK
	}
	
	public FileSyncConnection(SynchronisedFile syncFile, Socket socket) {
		super(socket);
		this.syncFile = syncFile;
	}
}