package filesync;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.kohsuke.args4j.Option;

/**
 * Server used in a FileSync
 * @author jordansteele
 *
 */

public class FileSyncServer extends FileSyncComponent {
	
	public static int port = 1553;
	
	@Option(name="-file", usage="file to sync with the server")
	private String syncFilename;
	
	@Option(name="-port", usage="port for communication")
	private int portArg = 1553;
	
	public static void main (String args[]) {
		try {
			new FileSyncServer().doMain(args);
		} catch (IOException e) {
			System.err.println("Failed to read args.");
			System.exit(-1);
		}
	}
	
	/**
	 * Does the main actions on an actual object rather than as static, 
	 * this is required for args4j
	 * @param args The command line arguments
	 */
	public void doMain(String[] args) throws IOException {
		parseArgs(this, args);
		startServerAndListen(port);
	}
	
	/**
	 * Overrides the superclass method to init the file sync conditions as well
	 */
	@Override
	protected void parseArgs(Object mainObject, String[] args) {
		super.parseArgs(mainObject, args);
		fileSyncConditions = new FileSyncConditions();
		port = portArg;
	}
	
	/**
	 * Creates a socket to listen on a port, continuously creates a new thread to 
	 * handle new clients when they make contact
	 * @param port
	 */
	private void startServerAndListen(int port) {
		try {
			ServerSocket listenSocket = new ServerSocket(port);
			while(true) {
				new Thread(new ClientInitConnection(listenSocket.accept())).start();
			}
		} 
		catch(IOException e) {
			System.out.println("Listen socket:"+e.getMessage());
		}
	}
	
	/**
	 * Inner class to handle setting up the server.
	 * Waits for the client to send through the file sync conditions, then
	 * starts the server as a destination or source depending on these conditions
	 * @author jordansteele
	 *
	 */
	public class ClientInitConnection extends Connection {
		public ClientInitConnection(Socket socket) {
			super(socket);
		}
		
		@Override
		public void run() {
			try {
				readMsgAndUpdateConditions();
				startConnection();
			} catch (IOException e) {
				System.out.println("failed and stuff");
			}
		}
		
		private void readMsgAndUpdateConditions() throws IOException{
			String msg = in.readUTF();
			fileSyncConditions.fromJSON(msg);
		}

		/**
		 * Starts either a source or destination connection based on conditions
		 */
		private void startConnection() {
			switch (fileSyncConditions.getDirection()) {
				case PUSH: 
					startDestination(createSyncFile(syncFilename, 
							fileSyncConditions.getBlockSize()), socket);
					break;
				case PULL:
					startSource(createSyncFile(syncFilename, 
							fileSyncConditions.getBlockSize()), socket);
					break;
				default:
					System.err.println("No such direction");
					System.exit(-1);
					break;
			}
		}
	}
}
