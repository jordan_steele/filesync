package filesync;

import java.io.IOException;
import java.net.Socket;

/**
 * Thread to be run on the source side that waits on the instruction buffer
 * and sends the instructions to the destination. Operates under a reply-response
 * protocol and hence waits for a response. Responses from the destination are either
 * instruction processed or a request for a new block.
 * @author jordansteele
 *
 */

public class SourceConnection extends FileSyncConnection {
	private SourceState state;
	
	enum SourceState {
		NORMAL,
		WAITING_RESPONSE
	}
	
	public SourceConnection(SynchronisedFile syncFile, Socket socket) {
		super(syncFile, socket);
		state = SourceState.NORMAL;
	}
	
	@Override
	public void run() {
		// keep track of the last instruction for upgrading to a new block when needed
		Instruction lastInst = null;
		
		while (true) {
			switch (this.state) {
				case NORMAL:
					lastInst = waitNextInstAndSend();
					break;
			
				case WAITING_RESPONSE:
					lastInst = waitResponseAndHandle(lastInst);
					break;
					
				default:
					System.err.println("unknown source state");
					System.exit(-1);
					break;
			}
		}
	}
	
	/**
	 * Waits for an instruction in the instruction buffer, then sends the instruction
	 * to the destination and updates the state to waiting a response
	 * @return The instruction that was sent
	 */
	private Instruction waitNextInstAndSend() {
		Instruction inst;
		while((inst = syncFile.NextInstruction()) != null){
			try {
				out.writeUTF(inst.ToJSON());
				this.state = SourceState.WAITING_RESPONSE;
				break;
			} catch(IOException e) {
				System.out.println("source message:" + e.getMessage());
			} 
		}
		return inst;
	}
	
	/**
	 * Waits for a response from the destination, if instruction processed then
	 * resets the source state to normal. Otherwise if the response is a request for
	 * a new block, then updates the last instruction sent and sends to the destination
	 * @param lastInst The last instruction that was sent
	 * @return Either the same as the input or the input upgraded to a new block
	 */
	private Instruction waitResponseAndHandle(Instruction lastInst) {
		String msg;
		try {
			msg = in.readUTF();
			switch (FileSyncCommand.valueOf(msg)) {
				case INSTRUCTION_PROCESSED:
					this.state = SourceState.NORMAL;
					break;
				case REQUEST_NEW_BLOCK:
					lastInst = updateLastInstAndSend(lastInst);
					break;
				default:
					System.err.println("unknown file sync command");
					System.exit(-1);
					break;
			}
		} catch (IOException e) {
			System.out.println("Failed to read ");
		} 
		return lastInst;
	}
	
	/**
	 * Updates the last instruction sent to a new block and sends to the destination
	 * @param lastInst The last instruction sent
	 * @return The the last instruction upgraded to a NewBlockInstruction
	 */
	private Instruction updateLastInstAndSend(Instruction lastInst) {
		lastInst=new NewBlockInstruction((CopyBlockInstruction)lastInst);
		try {
			out.writeUTF(lastInst.ToJSON());
		} catch (IOException e) {
			System.err.println("source: " + e.getMessage());
		}
		return lastInst;
	}
}
